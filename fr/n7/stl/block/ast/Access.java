/**
 * 
 */
package fr.n7.stl.block.ast;

/**
 * Enumeration of the different declaration parameters used in the language
 * @author DUBOIS, GARIBAL, HOTTIN, LARATO
 *
 */
public enum Access {
	
	Private,

	Protected,

	Public;

	@Override
	public String toString() {
		switch (this) {
		case Private: return "private";
		case Protected: return "protected";
		case Public: return "public";
		default: throw new IllegalArgumentException( "The default case should never be triggered.");		
		}
	}

}
