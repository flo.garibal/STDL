/**
 * 
 */
package fr.n7.stl.block.ast;

/**
 * @author DUBOIS, GARIBAL, HOTTIN, LARATO
 */
public interface AttributeDeclaration extends ElementClasse {

    /**
	 * Provide the type of the attribute in a class.
	 * @return Type of the attribute.
	 */
    Type getType();
}
