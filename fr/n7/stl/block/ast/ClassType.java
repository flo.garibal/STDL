/**
 * 
 */
package fr.n7.stl.block.ast;

import java.util.List;

/**
 * Abstract Syntax Tree node specification for class type.
 * @author Marc Pantel
 *
 */
public interface ClassType extends Type {

    /**
     * Provides the name of the class
     * @return The name of the class as a String
     */
    String getName();

    /**
     * Provides a list of the types of each implemented interfaces
     * @return List of types of each implemented interfaces
     */
    List<InterfaceType> getImplementedInterfaces();

    /**
     * Provides the type of the extended class
     * @return Type of the extended class
     */
    ClassType getExtendedClass();

}
