/**
 * 
 */
package fr.n7.stl.block.ast;

import fr.n7.stl.util.MethodSymbolTable;

/**
 * AST node for the class declaration instruction.
 * @author DUBOIS, GARIBAL, HOTTIN, LARATO
 *
 */
public interface Classe extends Instruction, Declaration {

    Type getType();

	MethodSymbolTable getMethodSymbolTable();
	
}
