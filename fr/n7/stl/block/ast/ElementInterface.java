package fr.n7.stl.block.ast;

public interface ElementInterface extends Declaration, Instruction {


    /**
     * Indicates whether this class element is correctly typed
     * @return A boolean indicating if this element is correctly typed;
     */
    boolean checkType();
}
