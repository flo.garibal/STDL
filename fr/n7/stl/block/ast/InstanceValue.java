/**
 * 
 */
package fr.n7.stl.block.ast;

import fr.n7.stl.util.SymbolTable;

/**
 *
 */
public interface InstanceValue extends Expression {
	
	SymbolTable<Declaration> getLocalSymbolTable();

}
