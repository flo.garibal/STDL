/**
 * 
 */
package fr.n7.stl.block.ast;

import java.util.List;

/**
 * AST node for the interface declaration instruction.
 * @author DUBOIS, GARIBAL, HOTTIN, LARATO
 *
 */
public interface Interface extends Instruction, Declaration {

    /**
     * Returns the type associated to the interface
     * @return the type of the interface
     */
    public Type getType();
    
    /**
     * @return list Element interface
     */
    public List<ElementInterface> getElements();
}
