/**
 * 
 */
package fr.n7.stl.block.ast;

import java.util.List;

/**
 * Abstract Syntax Tree node specification for interface type.
 * @author Marc Pantel
 *
 */
public interface InterfaceType extends Type {
    /**
     * Provides a list of the types of each extended interfaces
     * @return List of types of each extended interfaces
     */
    List<InterfaceType> getExtendedInterfaces();

    /**
     * Provides a list of the types of each elements of the interface
     * @return List of types of each elements of the interface
     */
    List<Type> getInterfaceElements();


    /**
     * Provides the name of the interface
     * @return The name of the interface as a String
     */
    String getName();
}
