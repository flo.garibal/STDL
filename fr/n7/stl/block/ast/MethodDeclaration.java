/**
 * 
 */
package fr.n7.stl.block.ast;

import java.util.List;

/**
 * @author DUBOIS, GARIBAL, HOTTIN, LARATO
 */
public interface MethodDeclaration extends ElementClasse {

    /**
     * Provides the access restriction of the element inside the class
     * @return The access restriction of this element as an Access
     */
    Access getAccess();
    
    Type getResult();
    
    Type getType();
    
    List<Instruction> getBody();
    
    List<Type> getParameters();

    boolean implementsSignatureDeclaration(SignatureDeclaration _signatureDeclaration);

}
