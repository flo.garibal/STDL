package fr.n7.stl.block.ast;

import fr.n7.stl.tam.ast.Fragment;
import fr.n7.stl.tam.ast.Register;
import fr.n7.stl.tam.ast.TAMFactory;

import java.util.List;

/**
 * Created by guillaume on 01/06/17.
 */
public interface Program {

    /**
     * Add a class to the program
     * @param _classe the class to add
     */
    void addClasse(Classe _classe);

    /**
     * Add an interface to the program
     * @param _interface the interface to add
     */
    void addInterface(Interface _interface);

    /**
     * Get the interfaces of the program
     * @return A list of the interfaces of the program
     */
    List<Interface> getInterfaces();

    /**
     * Get the classes of the program, excluding the main class
     * @return A list of the classes of the program
     */
    List<Classe> getClasses();

    /**
     * Get the main class of the program
     * @return The main class of the program
     */
    public Classe getMainClass();

    /**
     * Checks if the program is correctly typed
     * @return A boolean indicating if the program is correctly typed
     */
    boolean checkType();

    /**
     * Allocates memory for the program
     * @param _register the register of the program
     * @param _offset the offset of the program
     * @return the allocated memory
     */
    int allocateMemory(Register _register, int _offset);

    /**
     * Builds the code of the program in TAM
     * @param _factory the TAMFactory used to build the code
     * @return The TAM code of the program
     */
    Fragment getCode(TAMFactory _factory);
}
