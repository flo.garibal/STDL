package fr.n7.stl.block.ast;

import java.util.List;

/**
 * Created by guillaume on 23/05/17.
 */
public interface SignatureDeclaration extends ElementInterface {

    /**
     * Provides the name of the method associated to the signature
     * @return The name of the signature as a String
     */
    String getName();

    /**
     * Provides the list of the types of the parameters of the method associated to the signature
     * @return A list of Type of the parameters
     */
    List<Type> getParameters();


    /**
     * Provides the return type of the method associated to the signature
     * @return The Type of the return of the method
     */
    Type getReturnType();

    /**
     * Checks whether this SignatureDeclaration is the same as an other
     * @param _other The other SignatureDeclaration to check
     * @return a boolean indicating if the two signatures are the same
     */
    boolean equals(Object _other);

    /**
     * Returns the representation of the SignatureDeclaration as a String
     * @return the SignatureDeclaration as a String
     */
    String toString();
}
