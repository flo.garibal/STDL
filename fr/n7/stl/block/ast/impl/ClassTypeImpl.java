package fr.n7.stl.block.ast.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import fr.n7.stl.block.ast.AtomicType;
import fr.n7.stl.block.ast.AttributeDeclaration;
import fr.n7.stl.block.ast.ClassType;
import fr.n7.stl.block.ast.ForbiddenDeclarationException;
import fr.n7.stl.block.ast.InterfaceType;
import fr.n7.stl.block.ast.MethodDeclaration;
import fr.n7.stl.block.ast.Type;

/**
 * Implementation of the Abstract Syntax Tree node for a class type.
 * @author Marc Pantel
 */
public class ClassTypeImpl implements ClassType{

    private String name;
	private List<InterfaceType> implementedInterfaces;
	private ClassType extendedClass;

	public ClassTypeImpl(String _name, Iterable<InterfaceType> _implementedInterfaces, ClassType _extendedClass) {
		this.name = _name;

        this.implementedInterfaces= new LinkedList<InterfaceType>();
        for (InterfaceType _implementedInterface : _implementedInterfaces) {
            this.implementedInterfaces.add(_implementedInterface);
        }

		this.extendedClass = _extendedClass;


	}

	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#equalsTo(fr.n7.stl.block.ast.Type)
	 */
	@Override
    public boolean equalsTo(Type _other) {
	    boolean equalsTo = false;

	    if (_other != null) {
	        if (_other instanceof ClassTypeImpl) {
	            equalsTo = this.name.equals(((ClassTypeImpl) _other).getName());
            }
        }

        return equalsTo;
    }




	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#compatibleWith(fr.n7.stl.block.ast.Type)
	 */
	@Override
    public boolean compatibleWith(Type _other) {
	    boolean compatibleWith = false;

	    if (_other != null) {
	        if (_other instanceof  ClassTypeImpl) {
	            compatibleWith = this.name.equals(((ClassTypeImpl) _other).getName());

	            compatibleWith = compatibleWith | this.extendedClass.equalsTo(((ClassTypeImpl) _other).getExtendedClass());
            }
        }

	    return compatibleWith;
    }



	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#merge(fr.n7.stl.block.ast.Type)
	 */
    @Override
    public Type merge(Type _other) {
        if (this.equalsTo(_other)) {
            return this;
        }
        else {
            return AtomicType.ErrorType;
        }
    }



	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#length(int)
	 */
	@Override
	public int length() {
		int length = 0;

        for (InterfaceType t : this.implementedInterfaces) {
            length += t.length();
        }

        length += this.extendedClass.length();
		
		return length;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
	    StringBuilder stringBuilder = new StringBuilder();

	    stringBuilder.append(this.name);


        return stringBuilder.toString();
	}

	
    public List<InterfaceType> getImplementedInterfaces() {
        return implementedInterfaces;
    }

    @Override
    public String getName() {
	    return this.name;
    }

    @Override
    public ClassType getExtendedClass() {
        return extendedClass;
    }

    
    

}
