/**
 * 
 */
package fr.n7.stl.block.ast.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import fr.n7.stl.block.ast.Expression;
import fr.n7.stl.block.ast.Type;

/**
 * Implementation of the Abstract Syntax Tree node for a function type.
 * @author Marc Pantel
 *
 */
public class MethodeTypeImpl implements Type {

	private Type result;
	private List<Type> parameters;

	public MethodeTypeImpl(Type _result, Iterable<Type> _parameters) {
		this.result = _result;
		this.parameters = new LinkedList<Type>();
		for (Type _exp : _parameters) {
			this.parameters.add(_exp);
		}
	}

	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#equalsTo(fr.n7.stl.block.ast.Type)
	 */
	@Override
	public boolean equalsTo(Type _other) {
		boolean equals = true;
		int i = 0;
		
		if (!(_other instanceof MethodeTypeImpl)) {
			equals = false;
		} else {
			
			if (!result.equals(((MethodeTypeImpl)_other).getResult())) {
				equals = false;
			} else {
				i = 0;
				while (equals && i < parameters.size()) {
					equals = equals & (parameters.get(i).equals(((MethodeTypeImpl)_other).getParameters().get(i)));
				}
			}
		}
		return equals;
	}

	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#compatibleWith(fr.n7.stl.block.ast.Type)
	 */
	@Override
	public boolean compatibleWith(Type _other) {
		boolean equals = true;
		int i = 0;
		
		if (!(_other instanceof MethodeTypeImpl)) {
			equals = false;
		} else {
			
			if (!result.compatibleWith(((MethodeTypeImpl)_other).getResult())) {
				equals = false;
			} else {
				i = 0;
				while (equals && i < parameters.size()) {
					equals = equals & (parameters.get(i).compatibleWith(((MethodeTypeImpl)_other).getParameters().get(i)));
				}
			}
		}
		return equals;	
	}

	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#merge(fr.n7.stl.block.ast.Type)
	 */
	@Override
	public Type merge(Type _other) {
		Type newResult = null;
		List<Type> newParameters = null;
        int i = 0;
		
		if (!compatibleWith(_other)) {
			
		} else {
			newResult = result.merge(((MethodeTypeImpl)_other).getResult());
			newParameters = new LinkedList<Type>();
			i = 0;
			while (i < parameters.size()) {
				newParameters.add(parameters.get(i).merge(((MethodeTypeImpl)_other).getParameters().get(i)));
			}			
		}
		return (new MethodeTypeImpl(newResult, newParameters));		
	}

	/* (non-Javadoc)
	 * @see fr.n7.stl.block.ast.Type#length(int)
	 */
	@Override
	public int length() {
		int length = result.length();
		for (Type t : this.parameters) {
			length += t.length();
		}
		return length;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String _result = "(";
		Iterator<Type> _iter = this.parameters.iterator();
		if (_iter.hasNext()) {
			_result += _iter.next();
			while (_iter.hasNext()) {
				_result += " ," + _iter.next();
			}
		}
		return ") -> " + this.result;
	}
	
	
	public List<Type> getParameters(){
		return parameters;
	}
	
	public Type getResult() {
		return result;
	}

}
