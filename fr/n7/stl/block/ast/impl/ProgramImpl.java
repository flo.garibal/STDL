package fr.n7.stl.block.ast.impl;

import java.util.List;
import java.util.LinkedList;

import fr.n7.stl.block.ast.Classe;
import fr.n7.stl.block.ast.Interface;
import fr.n7.stl.block.ast.Program;
import fr.n7.stl.tam.ast.Fragment;
import fr.n7.stl.tam.ast.Register;
import fr.n7.stl.tam.ast.TAMFactory;


/**
 * Created by guillaume on 01/06/17.
 */

public class ProgramImpl implements Program {

    protected LinkedList<Interface> interfaces;
    protected LinkedList<Classe> classes;
    protected Classe mainClass;
    private int offset;


    public ProgramImpl(LinkedList<Interface> _interfaces, LinkedList<Classe> _classes, Classe _mainClass)
    {
        this.interfaces=_interfaces;
        this.classes=_classes;
        this.mainClass = _mainClass;
    }

    @Override
    public void addClasse(Classe _classe){
        this.classes.add(_classe);
    }

    @Override
    public void addInterface(Interface _interface){
        this.interfaces.add(_interface);
    }

    @Override
    public List<Interface> getInterfaces() {
        return this.interfaces;
    }

    @Override
    public List<Classe> getClasses() {
        return this.classes;
    }


    @Override
    public Classe getMainClass() {
        return this.mainClass;
    }


    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        for (Interface inter : this.interfaces) {
             sb.append(inter);
             sb.append('\n');
        }

        sb.append('\n');

        for (Classe classe : this.classes) {
            sb.append(classe.toString());
            sb.append('\n');
        }

        sb.append('\n');

        sb.append(this.mainClass.toString());

        return sb.toString();
    }

    @Override
    public boolean checkType()
    {
        boolean result = true;

        for (Interface inter : this.interfaces) {
            result = result && inter.checkType();
        }

        for (Classe classe : this.classes) {
            result = result && classe.checkType();
        }

        result = result && mainClass.checkType();

        return result;
    }

    @Override
    public int allocateMemory(Register _register, int _offset) {
        int localOffset = _offset;

        for (Interface inter : this.interfaces) {
            localOffset += inter.allocateMemory(_register, localOffset);
        }
        for (Classe classe : this.classes) {
            localOffset += classe.allocateMemory(_register, localOffset);
        }

        localOffset += this.mainClass.allocateMemory(_register, localOffset);

        this.offset = localOffset - _offset;

        return 0;
    }

    @Override
    public Fragment getCode(TAMFactory _factory) {
        Fragment fragment = _factory.createFragment();
//        for (Classe c : this.classes) {
//            for (Attribut as : c.getAttributsStatiques()) {
//                code.append(as.getCode(_factory));
//            }
//        }
//        code.append(princ.getCode(_factory));
//        for (Interface i : this.interfaces) {
//            code.append(i.getCode(_factory));
//        }
//        for (Classe c : this.classes) {
//            code.append(c.getCode(_factory));
//        }
        return fragment;
    }

}