package fr.n7.stl.block.ast.impl;

import fr.n7.stl.block.ast.Access;
import fr.n7.stl.block.ast.SignatureDeclaration;
import fr.n7.stl.block.ast.Type;
import fr.n7.stl.tam.ast.Fragment;
import fr.n7.stl.tam.ast.Register;
import fr.n7.stl.tam.ast.TAMFactory;

import java.util.List;

/**
 * Created by guillaume on 23/05/17.
 */
public class SignatureDeclarationImpl implements SignatureDeclaration {


    private String name;
    private List<Type> parameters;
    private Type returnType;

    public SignatureDeclarationImpl(String _name, List<Type> _parameters, Type _returnType) {
        this.name = _name;
        this.parameters = _parameters;
        this.returnType = _returnType;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Type> getParameters() {
        return this.parameters;
    }


    @Override
    public Type getReturnType() {
        return this.returnType;
    }

    @Override
    public boolean equals(Object _other) {
        boolean result = false;
        int listSize;
        int i;

        if (_other instanceof SignatureDeclaration) {
            result = this.name.equals(((SignatureDeclaration) _other).getName());
            result = result & this.returnType.equalsTo(((SignatureDeclaration) _other).getReturnType());

            if (this.parameters.size() == ((SignatureDeclaration) _other).getParameters().size()) {
                listSize = this.parameters.size();

                i = 0;
                while (i < listSize) {
                    result = result & this.parameters.get(i).compatibleWith(((SignatureDeclaration) _other).getParameters().get(i));
                    i++;
                }
            } else {
                result = false;
            }
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(returnType + " ");
        stringBuilder.append(this.name + "(");

        int i = 0;
        while (i < this.parameters.size()) {
            stringBuilder.append(this.parameters.get(i).toString());

            if (i != this.parameters.size() - 1) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append(");");

        return stringBuilder.toString();
    }


    @Override
    public boolean checkType() {
        return true;
    }

    //TODO
    @Override
    public int allocateMemory(Register _register, int _offset) {
        return 0;
    }
    //TODO
    @Override
    public Fragment getCode(TAMFactory _factory) {
        return null;
    }

}
