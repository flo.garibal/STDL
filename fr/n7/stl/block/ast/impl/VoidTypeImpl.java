package fr.n7.stl.block.ast.impl;

import fr.n7.stl.block.ast.AtomicType;
import fr.n7.stl.block.ast.Type;
import fr.n7.stl.block.ast.VoidType;

/**
 * Created by guillaume on 23/05/17.
 */
public class VoidTypeImpl implements VoidType {

    public VoidTypeImpl() {
    }

    @Override
    public boolean equalsTo(Type _other) {
        return (_other instanceof VoidType);
    }

    @Override
    public boolean compatibleWith(Type _other) {
        return (_other instanceof VoidType);
    }

    @Override
    public Type merge(Type _other) {
        if (this.compatibleWith(_other)) {
            return this;
        } else {
            return AtomicType.ErrorType;
        }
    }

    @Override
    public int length() {
        return 0;
    }

	@Override
	public String toString() {
		return "void";
	}
    
    
}
