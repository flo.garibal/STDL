package fr.n7.stl.util;

import java.util.*;

import fr.n7.stl.block.ast.*;
import fr.n7.stl.block.ast.impl.MethodDeclarationImpl;

/**
 * @author pantel2
 *
 */
public class MethodSymbolTable {

	private List<MethodDeclaration> methodDeclarations;
	private Optional<MethodSymbolTable> context;

	public MethodSymbolTable() {
		this.methodDeclarations = new ArrayList<MethodDeclaration>();
		this.context = Optional.empty();
	}
	
	public MethodSymbolTable(MethodSymbolTable _context) {
		this();
		if (_context != null) {
			this.context = Optional.of(_context);
		}
	}

	public Optional<MethodDeclaration> get(MethodDeclaration _signature) {
		if (this.methodDeclarations.contains(_signature)) {
		    int i = 0;
		    boolean stop = false;
		    MethodDeclaration signature = null;

		    while (!stop && i < this.methodDeclarations.size()) {
		        signature = this.methodDeclarations.get(i);
		        if (signature.equals(_signature)) {
		            stop = true;
                }
            }

			return Optional.of(signature);
		} else {
			if (this.context.isPresent()) {
				return this.context.get().get(_signature);
			} else {
				return Optional.empty();
			}
		}
	}

	public boolean contains(MethodDeclaration _signature) {
		// System.out.println("Contains( " + _name + " )");
		int i = 0;
		boolean result = false;
		
		while (!result && i < this.methodDeclarations.size()) {
		    result = this.methodDeclarations.get(i).equals(_signature);
		    i++;
        }

        return result;
	}

	public boolean accepts(MethodDeclaration _signature) {
		// System.out.println("Accepts( " + _declaration.getName() + " )");
		return (! this.contains(_signature));
	}

	public void register(MethodDeclaration _signature) throws ForbiddenDeclarationException {
		// System.out.println("Register( " + _declaration.getName() + " )");
		if (this.accepts(_signature)) {
			this.methodDeclarations.add(_signature);
		} else {
			throw new ForbiddenDeclarationException();
		}
	}

	public boolean knows(MethodDeclaration _signatureDeclaration) {
		// System.out.println("Knows( " + _name + " )");
		if (this.contains(_signatureDeclaration)) {
			return true;
		} else {
			if (this.context.isPresent()) {
				return this.context.get().knows(_signatureDeclaration);
			} else {
				return false;
			}
		}
	}

	public boolean knows(String _name, List<Expression> _arguments) {
        List<Type> parameters = new ArrayList<Type>();

        for (Expression e : _arguments) {
            parameters.add(e.getType());
        }

        MethodDeclaration methodDeclaration = new MethodDeclarationImpl(_name, null, parameters, null, Access.Public, false);

	    return this.knows(methodDeclaration);
	}

    public Optional<MethodDeclaration> get(String _name, List<Expression> _arguments) {
        List<Type> parameters = new ArrayList<Type>();

        for (Expression e : _arguments) {
            parameters.add(e.getType());
        }

        MethodDeclaration methodDeclaration = new MethodDeclarationImpl(_name, null, parameters, null, Access.Public, false);

        return this.get(methodDeclaration);
    }

	public String toString() {
		String _local = "";
		if (this.context.isPresent()) {
			_local += "Hierarchical definitions :\n" + this.context.get().toString();
		}
		_local += "Local definitions : ";
		for (MethodDeclaration signature : this.methodDeclarations) {
			_local += signature.toString() + "\n";
		}
		return _local;
	}

}
