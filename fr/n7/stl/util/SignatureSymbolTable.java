/**
 * 
 */
package fr.n7.stl.util;

import fr.n7.stl.block.ast.ForbiddenDeclarationException;
import fr.n7.stl.block.ast.SignatureDeclaration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author pantel2
 *
 */
public class SignatureSymbolTable {

	private List<SignatureDeclaration> signatureDeclarations;
	private Optional<SignatureSymbolTable> context;

	public SignatureSymbolTable() {
		this.signatureDeclarations = new ArrayList<SignatureDeclaration>();
		this.context = Optional.empty();
	}

	public SignatureSymbolTable(SignatureSymbolTable _context) {
		this();
		if (_context != null) {
			this.context = Optional.of(_context);
		}
	}

	public Optional<SignatureDeclaration> get(SignatureDeclaration _signature) {
		if (this.signatureDeclarations.contains(_signature)) {
		    int i = 0;
		    boolean stop = false;
		    SignatureDeclaration signature = null;

		    while (!stop && i < this.signatureDeclarations.size()) {
		        signature = this.signatureDeclarations.get(i);
		        if (signature.equals(_signature)) {
		            stop = true;
                }
		        i++;
            }

			return Optional.of(signature);
		} else {
			if (this.context.isPresent()) {
				return this.context.get().get(_signature);
			} else {
				return Optional.empty();
			}
		}
	}

	public boolean contains(SignatureDeclaration _signature) {
		// System.out.println("Contains( " + _name + " )");
		int i = 0;
		boolean result = false;

		while (!result && i < this.signatureDeclarations.size()) {
		    result = this.signatureDeclarations.get(i).equals(_signature);
		    i++;
        }

        return result;
	}

	public boolean accepts(SignatureDeclaration _signature) {
		// System.out.println("Accepts( " + _declaration.getName() + " )");
		return (! this.contains(_signature));
	}

	public void register(SignatureDeclaration _signature) throws ForbiddenDeclarationException {
		// System.out.println("Register( " + _declaration.getName() + " )");
		if (this.accepts(_signature)) {
			this.signatureDeclarations.add(_signature);
		} else {
			throw new ForbiddenDeclarationException();
		}
	}

	public boolean knows(SignatureDeclaration _signatureDeclaration) {
		// System.out.println("Knows( " + _name + " )");
		if (this.contains(_signatureDeclaration)) {
			return true;
		} else {
			if (this.context.isPresent()) {
				return this.context.get().knows(_signatureDeclaration);
			} else {
				return false;
			}
		}
	}

	public String toString() {
		String _local = "";
		if (this.context.isPresent()) {
			_local += "Hierarchical definitions :\n" + this.context.get().toString();
		}
		_local += "Local definitions : ";
		for (SignatureDeclaration signature : this.signatureDeclarations) {
			_local += signature.toString() + "\n";
		}
		return _local;
	}

}
