//test class, classe avec tableau, while, if, for
//OK
public class Test {

	private int[] tableau;
	
	public Test(int taille){
		tableau = new int[taille];
	}
	
	public int[] getTableau() {
		return tableau;
	}
	
	public int geti (int i) {
		return tableau[i];
	}

	public void setTableau(int[] t) {
		tableau = t;
	}
	
	public void seti(int i, int val) {
		tableau[i] = val;
	}

	public static void main (String[] args){
		Test t = new Test(10);
		int k;
		if (true){
			for (k=0; k<10; k++){
				t.seti(k, k);;
			}
		}
		k = 0;
		while (k<10){
			t.seti(k, k+1);
			k++;
		}

		
	}
}