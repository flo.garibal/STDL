// test interface, attribut public non static

interface Inter {
	final static int test = 25;
	int getEntier();
	int setEntier();
}

class A implements Inter{
	public int myInt;

	public int getEntier(){
		return myInt;
	}

	public int setEntier(){
		return myInt;
	}
}


public class MaClass{
	public static void main (String[] args){

	}
}